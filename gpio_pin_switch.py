#!/usr/bin/env python3
"""Enables toggling/holding closed (powered) a GPIO pin."""

import argparse
import time

import RPi.GPIO as gpio


def main():
    """Entry point."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-p', '--pin-no', required=True, type=int, help='GPIO pin to affect')
    parser.add_argument(
        '-t', '--toggle', choices=['on', 'off'], help='Toggle pin on/off')
    parser.add_argument(
        '-hc', '--hold-closed', type=float,
        help='Hold switch closed for n seconds')
    args = parser.parse_args()

    gpio.setmode(gpio.BCM)
    gpio.setup(args.pin_no, gpio.OUT)

    if args.toggle:
        # If toggling must not cleanup
        toggle = args.toggle == 'on'
        gpio.output(args.pin_no, toggle)
    elif args.hold_closed:
        # Hold closed temp
        gpio.output(args.pin_no, True)
        time.sleep(args.hold_closed)
        gpio.output(args.pin_no, False)
        # If not toggling, can cleanup now
        gpio.cleanup()


if __name__ == '__main__':
    main()
